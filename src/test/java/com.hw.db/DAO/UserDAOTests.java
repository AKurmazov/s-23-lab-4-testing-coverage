package com.hw.db.DAO;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;


public class UserDAOTests {
    private JdbcTemplate jbdcMock;
    private UserDAO userDAO;
    private User user;

    private final String SQL1 = "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;";
    private final String SQL2 = "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;";
    private final String SQL3 = "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;";

    @BeforeEach
    void setUp() {
        jbdcMock = Mockito.mock(JdbcTemplate.class);
        userDAO = new UserDAO(jbdcMock);
        user = new User("nickname", null, null, null);
    }

    @Test
    @DisplayName("Test Change with SQL1")
    void testChangeSQL1() {
        user.setEmail("email");
        userDAO.Change(user);
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL1), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    @DisplayName("Test Change with SQL2")
    void testChangeSQL2() {
        user.setFullname("fullname");
        userDAO.Change(user);
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL2), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    @DisplayName("Test Change with SQL3")
    void testChangeSQL3() {
        user.setAbout("about");
        userDAO.Change(user);
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL3), Mockito.anyString(), Mockito.anyString());
    }
}