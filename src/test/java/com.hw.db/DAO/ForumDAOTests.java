package com.hw.db.DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;


public class ForumDAOTests {
    private JdbcTemplate jbdcMock;
    private ForumDAO forumDAO;

    private final String SQL1 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext ORDER BY nickname;";
    private final String SQL2 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext ORDER BY nickname desc;";
    private final String SQL3 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;";
    private final String SQL4 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;";
    private final String SQL5 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;";
    private final String SQL6 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";
    private final String SQL7 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;";
    private final String SQL8 = "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;";

    @BeforeEach
    void setUp() {
        jbdcMock = Mockito.mock(JdbcTemplate.class);
        forumDAO = new ForumDAO(jbdcMock);
    }

    @Test
    @DisplayName("Test UserList with SQL1")
    void testUserListSQL1() {
        forumDAO.UserList("slug", null, null, null);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL1), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL2")
    void testUserListSQL2() {
        forumDAO.UserList("slug", null, null, true);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL2), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL3")
    void testUserListSQL3() {
        forumDAO.UserList("slug", 1, null, null);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL3), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL4")
    void testUserListSQL4() {
        forumDAO.UserList("slug", 1, null, true);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL4), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL5")
    void testUserListSQL5() {
        forumDAO.UserList("slug", null, "since", true);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL5), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL6")
    void testUserListSQL6() {
        forumDAO.UserList("slug", 1, "since", true);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL6), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL7")
    void testUserListSQL7() {
        forumDAO.UserList("slug", null, "since", null);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL7), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Test UserList with SQL8")
    void testUserListSQL8() {
        forumDAO.UserList("slug", 1, "since", null);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL8), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}