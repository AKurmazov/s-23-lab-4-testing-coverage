package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;


public class ThreadDAOTests {
    private JdbcTemplate jbdcMock;
    private ThreadDAO threadDAO;

    private final String SQL1 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;";
    private final String SQL2 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;";

    @BeforeEach
    void setUp() {
        jbdcMock = Mockito.mock(JdbcTemplate.class);
        threadDAO = new ThreadDAO(jbdcMock);
    }

    @Test
    @DisplayName("Test treeSort with SQL1")
    void testTreeSortSQL1() {
        threadDAO.treeSort(1, null, 5, true);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL1), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
    }

    @Test
    @DisplayName("Test treeSort with SQL2")
    void testTreeSortSQL2() {
        threadDAO.treeSort(1, 5, 10, null);
        Mockito.verify(jbdcMock).query(Mockito.eq(SQL2), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
    }
}