package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;


public class PostDAOTests {
    private JdbcTemplate jbdcMock;
    private PostDAO postDAO;
    private Post post;
    private Timestamp ts;

    private final String SQL1 = "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;";
    private final String SQL2 = "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;";
    private final String SQL3 = "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
    private final String SQL4 = "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;";
    private final String SQL5 = "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
    private final String SQL6 = "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
    private final String SQL7 = "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";

    @BeforeEach
    void setUp() {
        jbdcMock = Mockito.mock(JdbcTemplate.class);
        postDAO = new PostDAO(jbdcMock);

        ts = Timestamp.valueOf(LocalDateTime.of(2021, Month.JANUARY, 1, 0, 0));
        post = new Post("author", ts, "forum", "message", 1, 1, false);

        Mockito.when(
            jbdcMock.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any()
            )
        ).thenReturn(post);
    }

    @Test
    @DisplayName("Test setPost with SQL1")
    void testSetPostSQL1() {
        postDAO.setPost(1, new Post("new_author", ts, "forum", "message", 1, 1, false));
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL1), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("Test setPost with SQL2")
    void testSetPostSQL2() {
        postDAO.setPost(1, new Post(null, ts, "forum", "new_message", 1, 1, false));
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL2), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("Test setPost with SQL3")
    void testSetPostSQL3() {
        postDAO.setPost(1, new Post(
                "author", Timestamp.valueOf(LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0)), "forum", "message", 1, 1, false
            )
        );
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL3), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("Test setPost with SQL4")
    void testSetPostSQL4() {
        postDAO.setPost(1, new Post("new_author", ts, "forum", "new_message", 1, 1, false));
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL4), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("Test setPost with SQL5")
    void testSetPostSQL5() {
        postDAO.setPost(1, new Post(
                "new_author", Timestamp.valueOf(LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0)), "forum", "message", 1, 1, false
            )
        );
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL5), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("Test setPost with SQL6")
    void testSetPostSQL6() {
        postDAO.setPost(1, new Post(
                "author", Timestamp.valueOf(LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0)), "forum", "new_message", 1, 1, false
            )
        );
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL6), Optional.ofNullable(Mockito.any()));
    }

    @Test
    @DisplayName("Test setPost with SQL7")
    void testSetPostSQL7() {
        postDAO.setPost(1, new Post(
                "new_author", Timestamp.valueOf(LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0)), "forum", "new_message", 1, 1, false
            )
        );
        Mockito.verify(jbdcMock).update(Mockito.eq(SQL7), Optional.ofNullable(Mockito.any()));
    }
}